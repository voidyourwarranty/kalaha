#include <stdio.h>
#include <assert.h>

#define NDEBUG   // removes all assertions from the source code

#define WIDTH 6  // the size of the board: the number of pits on each side of the board
#define COUNT 4  // the initial number of stones per pit

#define DEPTH 10 // the depth (number of plies) used in the valuation

// The player, sometimes used as an array index.
typedef enum { south=0, north=1 } player;

// The board configuration.
// The first index is the player number,
// the second index is the number of the pit on this player's side where zero is the Kalaha and 1..WIDTH are the
//   other pits numbered by their distance from rhe Kalaha.
typedef unsigned int board[2][WIDTH+1];

// The state of the game.
// <b> - the board configuration.
// <t> - the player who will have got the next turn.
typedef struct {
  board  b;
  player t;
} state;

// A valuation of a position.
// <s> - the state of the game (board configurationn and player with the next turn).
// <p> - the player from whose point of view the position is valued.
// <v> - the resulting value (a certain gain or a certain loss is denoted by
//       plus or minus the total number of stones of the game).
typedef struct {
  state  s;
  player p;
  int    v;
} valuation;

#ifndef NDEBUG
// Check whether <p> is a valid player.
static int IsPlayer ( player p ) {
  return ((p == south) || (p == north));
}
#endif

// Return the player other than <p>.
static player OtherPlayer ( player p ) {
#ifndef NDEBUG
  assert (IsPlayer (p));
#endif
  return ((p + 1) % 2);
}

// Set the board configuration <s> to the initial configuration of the game.
void BoardInitial ( board *s ) {
  assert (s);
  for (int i=0;i<2;i++) {
    (*s)[i][0] = 0;
    for (int j=1;j<=WIDTH;j++)
      (*s)[i][j] = COUNT;
  }
}

// Copy the board configuration <t> to <s>.
void BoardCopy ( board *s, board *t ) {
  assert (s);
  assert (t);
  for (int i=0;i<2;i++)
    for (int j=0;j<=WIDTH;j++)
      (*s)[i][j] = (*t)[i][j];
}

// Print the board configuration <s>.
void BoardPrint ( board *s ) {
  assert (s);
  printf ("    ");
  for (int i=1;i<=WIDTH;i++) {
    printf ("[%2d]",(*s)[1][i]);
  }
  printf ("\n[%2d]",(*s)[1][0]);
  for (int i=1;i<=WIDTH;i++) {
    printf ("    ");
  }
  printf ("[%2d]\n    ",(*s)[0][0]);
  for (int i=WIDTH;i>0;i--) {
    printf ("[%2d]",(*s)[0][i]);
  }
  printf ("\n");
}

// Copy the valuation <t> to <s>.
void ValuationCopy ( valuation *s, valuation *t ) {
  BoardCopy (&s->s.b,&t->s.b);
  s->s.t = t->s.t;
  s->p = t->p;
}

// Print the name of the player <p>.
void PrintPlayer ( player p ) {
#ifndef NDEBUG
  assert (IsPlayer (p));
#endif
  if (p == south) {
    printf ("south");
  } else {
    printf ("north");
  }
}

#ifndef NDEBUG
// Count the total number of stones of the board configuration <s>. This is used
// only for consistency checks as the number if stones remains a constant.
unsigned int TotalStones ( board *s ) {
  unsigned int i=0;
  for (int j=0;j<=1;j++) {
    for (int k=0;k<=WIDTH;k++) {
      i += (*s)[j][k];
    }
  }

  return (i);
}
#endif

// Count the number of active stones, i.e. the number of stones not in any of the two Kalahas.
unsigned int ActiveStones ( board *s ) {
  unsigned int i=0;
  for (int j=0;j<=1;j++) {
    for (int k=1;k<=WIDTH;k++)
      i += (*s)[j][k];
  }

  return (i);
}

// Given a board configuration <s> and a player <t>, is emptying the pit number <p> a legal move.
int IsLegalMove ( board *s, player t, int p ) {
  assert (s);
#ifndef NDEBUG
  assert (IsPlaer (t));
#endif
  if ((p < 1) || (p > WIDTH))
    return (0);

  return ((*s)[t][p] > 0);
}

// Perform a single move.
// <s> - state of the game (to be modified by the move).
// <p> - the position to be emptied (on the side of the player whose turn it is according to <s>).
void Move ( state *s, int p ) {
  assert (s);
#ifndef NDEBUG
  assert (IsPlayer (s->t));
#endif
  assert (IsLegalMove (&s->b,s->t,p));

  player c = s->t;               // on whose side of the board does the move begin?
  int    h = (s->b)[s->t][p];    // number of stones from the pit at which the mobe begins, now in the hand of the player
  (s->b)[s->t][p] = 0;           // this pit is now empty
  player n = OtherPlayer (s->t); // the player who will have the next turn

  while (h) {
    // As long as there are stones to drop...

    if (p > 1) {
      // If the current pit is not next to a Kalaha, but next to an ordinary other pit...

      if ((h == 1) && (c == s->t) && ((s->b)[c][p-1] == 0) && ((s->b)[OtherPlayer (c)][WIDTH+2-p] > 0)) {
	// If the last stone is to be dropped (h==1) and the position of the current pit is on the side of the
	// player whose turn it is (c==t) and the next pit (into which the last stone is to be dropped) is empty and
	// the opposite pit contains stones, these stones are captured.
	(s->b)[s->t][0] += 1+(s->b)[OtherPlayer (c)][WIDTH+2-p]; // the last stone and the captured stones are added to the player's Kalaha
	(s->b)[OtherPlayer (c)][WIDTH+2-p] = 0;                  // the captured stones are gone
	h = 0;                                                   // the hand is empty (in this case <p> does not matter)
      } else {
	// This is a normal dropping of stones.

	(s->b)[c][--p]++; // one stone is dropped into the adjacent pit, the current pit is set to the adjacent one,
	h--;              // and there is one stone less in the player's hand
      }
    } else if (p == 1) {
      // If the current pit is next to a Kalaha...

      if (s->t == c) {
	// If the current side of the board is the side of the player whose turn it is...

	(s->b)[c][0]++;        // drop the stone into the Kalaha
	if (h > 1) {
	  // If this was not the last stone...

	  c = OtherPlayer (c); // the move continues on the other side of the board
	  p = WIDTH+1;         // ... with the pit far away from the Kalaha
	} else {
	  // If this was the last stone...

	  n = s->t;            // the same player has got another turn
	}

	h--;                   // in both cases, the number of stones in the hand of the player is reduced
      } else {
	// If the current side of the board is not the side of the player whose turn it is...

	if ((h == 1) && (OtherPlayer (c) == s->t) && ((s->b)[OtherPlayer (c)][WIDTH] == 0) && ((s->b)[c][1] > 0)) {
	  // If the last stone is to be dropped (h==1) and the position of the current pit is on the side opposite to
	  // the player whose turn it is (OtherPlayer (c) == t) and the next pit (into which the last stone is to be
	  // dropped) is empty and the opposite pit contains stones, these stones are captured.

	  (s->b)[s->t][0] += 1+(s->b)[c][1];
	  (s->b)[c][1] = 0;
	  h = 0;
	} else {
	  // This is the normal dropping of stones.

	  (s->b)[OtherPlayer (c)][WIDTH]++; // the stone is dropped into the pit on the other side of the board farthest from the Kalaha
	  h--;                              // the number of stones in the player's hand is reduced
	  p = WIDTH;                        // the next pit is the farthest from the Kalaha
	  c = OtherPlayer (c);              // ... on the other side of the board
	}
      }
    }

    if (h == 0) {
      // If the move is about to finish because the hand is empty, we check if the player's side of the board is empty.

      int i = 1;
      for (int j=1;j<=WIDTH;j++) {
	if ((s->b)[s->t][j] > 0) {
	  i = 0;
	  break;
	}
      }
      if (i) {
	// If the player's side of the board is empty, all stones on the other side are put into the other player's Kalaha.

	for (int j=1;j<=WIDTH;j++) {
	  (s->b)[OtherPlayer (s->t)][0] += (s->b)[OtherPlayer (s->t)][j];
	  (s->b)[OtherPlayer (s->t)][j] = 0;
	}
      }
    }
  }

#ifndef NDEBUG
  assert (TotalStones (&s->b) == 2*WIDTH*COUNT); // The total number of stones has to remain constant.
#endif
  s->t = n;
}

// Has the game already been decided?
int IsDecided ( board *s ) {
  return ((ActiveStones (s) == 0) || ((*s)[south][0] > WIDTH*COUNT) || ((*s)[north][0] > WIDTH*COUNT));
}

// Compute a valuation of a state of the game.
// <s> - s->s is the state of the game (board and player who has got the next turn).
//     - s->p is the player from whose point of view the valuation is calculated.
//     - s->v is where the resulting value is stored.
// <d> - number of plies to calculate in depth
// <n> - counts the number of positions analyzed
//
// At depth zero (no more recursive analysis), the valuation is the diffrence of the number of stones in
// s->p's Kalaha minus the number of stones in the opponents Kalaha.
//
// At depth higher than zero, we always assume that the evaluating player s->p performs the best available move whereas
// the opponent performs the move that is least advantageous for the evaluating player.
void ComputeValuation ( valuation *s, unsigned int d, unsigned long int *n ) {

  int       max,min;
  valuation v;

  if (d) {
    // At depth higher than zero, consider all legal moves and proceed recursively.

    max = -2*WIDTH*COUNT; // will contain the highest possible valuation from s->p's point of view
    min = 2*WIDTH*COUNT;  // ... the lowest possible valuation

    for (int i=1;i<=WIDTH;i++) {
      if (IsLegalMove (&s->s.b,s->s.t,i)) {

	(*n)++;
	ValuationCopy (&v,s);
	Move (&v.s,i);
	ComputeValuation (&v,d-1,n);

	if (v.v < min)
	  min = v.v;
	if (v.v > max)
	  max = v.v;
      }
    }

    if (s->p == s->s.t) {
      s->v = max; // If it is the evaluating player's turn, we assume the most advantageous move.
    } else {
      s->v = min; // If it is the opponent's turn, we assume the most disadvantageous move (for the evaluating player).
    }

  } else {

    if (s->s.b[s->p][0] > WIDTH*COUNT) // certain win
      s->v = 2*WIDTH*COUNT;
    else if (s->s.b[OtherPlayer (s->p)][0] > WIDTH*COUNT) // certain loss
      s->v = -2*WIDTH*COUNT;
    else
      s->v = s->s.b[s->p][0] - s->s.b[OtherPlayer (s->p)][0]; // valuation from difference of secure stones in the two Kalahas
  }
}

// Given the state of the game <s> and the evaluation depth <d>, print the legal moves and in parentheses their
// valuation from the point of view of the player who has got the next turn.
void PrintValuation ( state *s, unsigned int d ) {
  valuation v;
  unsigned long int n = 0;

  printf ("valuations: ");
  for (int i=1;i<=WIDTH;i++) {
    if (IsLegalMove (&s->b,s->t,i)){
      BoardCopy (&v.s.b,&s->b);
      v.s.t = s->t;
      Move (&v.s,i);
      v.p = s->t;
      ComputeValuation (&v,d,&n);
      printf ("%d (%d),",i,v.v);
    }
  }
  printf (" (computing %lu positions to depth %d)\n",n,d);
}

int main ( void ) {
  state s;
  int m;

  BoardInitial (&s.b);
  s.t = south;
  do{
    BoardPrint (&s.b);
    for (int i=0;i<WIDTH+2;i++) {
      printf ("----");
    }
    printf ("\n");
    PrintValuation (&s,DEPTH); // ??? fixed depth
    do{
      PrintPlayer (s.t);
      printf (" moves: ");
      scanf ("%d",&m);
    }while (!IsLegalMove (&s.b,s.t,m));
    Move (&s,m);
  }while (!IsDecided (&s.b));

  BoardPrint (&s.b);
  if (s.b[south][0] > s.b[north][0]) {
    printf ("south wins.\n");
  } else if (s.b[south][0] < s.b[north][0]) {
    printf ("north wins.\n");
  } else {
    printf ("draw.\n");
  }

  return (0);
}
