# kalaha : Game of Kalaha (Mancala). Recursive valuation of moves.

The board game Kalaha (or Kalah) is a variant of the well known [Mancala](https://en.wikipedia.org/wiki/Mancala)
games. This project is a simple and fast `C` implementation of a valuation of Kalaha positions, based on a simple
recursive depth search. You can play the game interactively, using the terminal as the game board (simple ASCII art),
and before each turn, the computer outputs the valuation (read: recommendation) of the possible moves.

## Overview

The Mancala variants covered here use an arbitrary number of pits on either side of the board and an arbitrary number
of stones per pit in the initial configuration. The following special rules apply:
* when the last stone of a move is dropped into the player's own Kalaha, that player gets another turn (possibly
  repeatedly),
* when the last stone of a move is dropped into an empty pit on the player's side *and* there are stones in the opposite
  pit, then these stones are 'captured', i.e. the last stone of the move and the captured stones can be placed into the
  player's own Kalaha. (Note that if the opposite pit contains no stones, nothing special happens.)

This variant of the Mancala games was subject of a systematic [brute force
analysis](http://graphics.stanford.edu/~irving/papers/irving2000_kalah.pdf) by computer scientists Geoffrey Irving,
Jeroen Donkers and Jos Uiterwijk in 2000. I am not aware of any abstract proof of their results though.

## Compilation

```
gcc -O3 -o kalaha kalaha.c
```

## Usage

The boards are displayed as follows. Here is the initial configuration of (6,4)-Kalaha (i.e. 6 pits on either side and initially 4 stones in each pit).
```
    [ 4][ 4][ 4][ 4][ 4][ 4]
[ 0]                        [ 0]
    [ 4][ 4][ 4][ 4][ 4][ 4]
```
The players are named North (upper row, left Kalaha) and South (lower row, right Kalaha). South begins. Stones are dropped in counter-clockwise direction. The moves are numbered by the distance of the pit that is to be emptied from the player's own Kalaha.

I.e. the famous first move by South is `4`, resulting in the configuration
```
    [ 4][ 4][ 4][ 4][ 4][ 4]
[ 0]                        [ 1]
    [ 4][ 4][ 0][ 5][ 5][ 5]
```

After displaying the new configuration, the computer suggests the valuations of the possible moves. For example, after the above move by South
it is South's turn again, and the computer recommends
```
valuations: 1 (5),2 (3),3 (-1),5 (-3),6 (-4), (computing 61582935 positions to depth 10)
south moves: ...
```

Under `valuations`, all possible moves 1,2,3,5,6 are listed with the valuation of their resulting configurations in
parentheses. The valuation is the best possible outcome that can be guaranteed regardless of the opponent's moves,
measured as the difference of the stones secured in the player's own Kalaha minus that in his opponent's Kalaha at the
maximum search depth. A certain win is indicated by the total number of stones on the board while a certain loss is
indicated by the negative total number of stones on the board (here: 2 times 6 times 4 = 48).

In the above example, the computer has evaluated partial turns up to a depth of 10, considering some 61 million
configurations. The best move recommended to South is `1` with valuation 5, i.e. picking the pit next to his own
Kalaha. With this move, if South plays optimally, it is guaranteed that South can achieve an advantage of 5 stones after
the 10 turns analyzed. Going for `3`, however, would be a mistake which would allow North to achieve an advantage of one
stone after 10 turns if North played optimally.

On my laptop (2019 version, 4.2 GHz Core i5 Intel mobile version), depth 10 takes no more than 5 seconds per move on a single core.

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
